PUT /jd_goods
{
   "settings" : {
      "number_of_shards" : 1,
      "number_of_replicas" : 1
   },
   "mappings": {
     "properties": {
       "id":{"type": "integer"},
       "title":{"type": "text","analyzer": "ik_max_word","search_analyzer": "ik_smart"},
         "desc":{"type": "text","analyzer": "ik_max_word","search_analyzer": "ik_smart"},
       "price":{"type": "text"},
       "img":{"type": "text"}
     }
   }
}


PUT /jd_goods/_doc/1
{
  "title":"女士包",
  "img":"http://xxx.png",
  "price":"22",
  "desc": "一顿操作猛如虎，一看工资2500"
}



GET jd_goods/_search
{
  "query": {
    "match": {
      "title": "女包"
    }
  }
}

POST /jd_goods/_mapping
{
        "properties": {
            "title": {
                "type": "text",
                "analyzer": "ik_max_word_pinyin",
                "search_analyzer": "ik_smart_pinyin"
            },
            "desc": {
                "type": "text",
               "analyzer": "ik_max_word_pinyin",
               "search_analyzer": "ik_smart_pinyin"
            },
            "img": {
                "type": "text"
            },
            "price": {
                "type": "text"
            }
        }
}


PUT /jd_goods
{
   "settings": {
        "analysis": {
            "analyzer": {
                "ik_smart_pinyin": {
                    "type": "custom",
                    "tokenizer": "ik_smart",
                    "filter": ["my_pinyin", "word_delimiter"]
                },
                "ik_max_word_pinyin": {
                    "type": "custom",
                    "tokenizer": "ik_max_word",
                    "filter": ["my_pinyin", "word_delimiter"]
                }
            },
            "filter": {
                "my_pinyin": {
                    "type" : "pinyin",
                    "keep_separate_first_letter" : true,
                    "keep_full_pinyin" : true,
                    "keep_original" : true,
                    "limit_first_letter_length" : 16,
                    "lowercase" : true,
                    "remove_duplicated_term" : true
                }
            }
        }
  }
}



GET _analyze?pretty
{
  "analyzer": "ik_smart",
  "text": "中华人民共和国"
}

GET _analyze?pretty
{
  "analyzer": "ik_max_word",
  "text": "中华人民共和国"
}


PUT /jd_goods
{
   "settings": {
        "analysis": {
            "analyzer": {
                "ik_smart_pinyin": {
                    "type": "custom",
                    "tokenizer": "ik_smart",
                    "filter": ["my_pinyin", "word_delimiter"]
                },
                "ik_max_word_pinyin": {
                    "type": "custom",
                    "tokenizer": "ik_max_word",
                    "filter": ["my_pinyin", "word_delimiter"]
                }
            },
            "filter": {
                "my_pinyin": {
                    "type" : "pinyin",
                    "keep_separate_first_letter" : true,
                    "keep_full_pinyin" : true,
                    "keep_original" : true,
                    "limit_first_letter_length" : 16,
                    "lowercase" : true,
                    "remove_duplicated_term" : true
                }
            }
        }
  },
    "mappings": {
        "properties": {
            "title": {
                "type": "text",
                "analyzer": "ik_max_word_pinyin",
                "search_analyzer": "ik_smart_pinyin"
            },
            "desc": {
                "type": "text",
               "analyzer": "ik_max_word_pinyin",
               "search_analyzer": "ik_smart_pinyin"
            },
            "img": {
                "type": "text"
            },
            "price": {
                "type": "text"
            }
        }
    }
}