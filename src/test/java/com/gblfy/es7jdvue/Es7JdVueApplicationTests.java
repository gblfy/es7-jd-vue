package com.gblfy.es7jdvue;

import com.alibaba.fastjson.JSON;
import com.gblfy.es7jdvue.pojo.User;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.core.TimeValue;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.FetchSourceContext;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * gblfy es7.15.2 高级客户端测试API
 */
@SpringBootTest
class Es7JdVueApplicationTests {

    //索引常量
    public static final String ES_INDEX = "gblfy";

    @Autowired
    @Qualifier("restHighLevelClient")
    private RestHighLevelClient client;

    // 测试索引的创建 Request
    @Test
    void testCreateIndex() throws IOException {
        //1. 创建索引请求
        CreateIndexRequest request = new CreateIndexRequest(ES_INDEX);
        //2. 客户端执行请求 IndexClient 请求后得到响应
        CreateIndexResponse createIndexResponse = client.indices().create(request, RequestOptions.DEFAULT);
        System.out.println(createIndexResponse);
    }

    // 测试获取索引,判断其是否存在
    @Test
    void testExistIndex() throws IOException {
        GetIndexRequest request = new GetIndexRequest(ES_INDEX);
        boolean exists = client.indices().exists(request, RequestOptions.DEFAULT);
        System.out.println(exists);
    }

    // 测试删除索引
    @Test
    void testDeleteIndex() throws IOException {
        DeleteIndexRequest request = new DeleteIndexRequest(ES_INDEX);
        // 删除
        AcknowledgedResponse delete = client.indices().delete(request, RequestOptions.DEFAULT);
        System.out.println(delete.isAcknowledged());
    }

    // 测试添加文档
    void testAddDocument() throws IOException {
        // 创建对象
        User user = new User("gblfy", 2);
        // 创建请求
        IndexRequest request = new IndexRequest(ES_INDEX);

        // 规则 PUT /gblfy/_doc/1
        request.id("1");
        request.timeout(TimeValue.timeValueSeconds(1));

        // 将我们的数据放入请求 json
        IndexRequest source = request.source(JSON.toJSONString(user), XContentType.JSON);

        // 客户端发送请求,获取相应的结果
        IndexResponse indexResponse = client.index(request, RequestOptions.DEFAULT);
        System.out.println("返回响应:" + indexResponse.toString());
        System.out.println("状态:" + indexResponse.status());
    }

    // 测试获取文档引,判断其是否存在 GET /index/_doc/1
    @Test
    void testIsExistDocument() throws IOException {
        GetRequest getRequest = new GetRequest(ES_INDEX, "9FP4en0BAvq0AoKtrp8G");
        //     不获取返回的上下文
        getRequest.fetchSourceContext(new FetchSourceContext(false));
        getRequest.storedFields("_onoe_");
        boolean exists = client.exists(getRequest, RequestOptions.DEFAULT);
        System.out.println(exists);
    }

    // 获取文档信息
    @Test
    void testGetDocument() throws IOException {
        GetRequest getRequest = new GetRequest(ES_INDEX, "1");
        GetResponse getResponse = client.get(getRequest, RequestOptions.DEFAULT);
        // 打印文档内容
        System.out.println(getResponse.getSourceAsString());
        // 这里返回的全部内容很命令是一样的
        System.out.println(getResponse);
    }

    // 更新文档
    @Test
    void testUpdateDocument() throws IOException {
        UpdateRequest updateRequest = new UpdateRequest(ES_INDEX, "1");
        updateRequest.timeout("1s");

        User user = new User("gbldy专家java", 18);
        updateRequest.doc(JSON.toJSONString(user), XContentType.JSON);
        UpdateResponse updateResponse = client.update(updateRequest, RequestOptions.DEFAULT);
        System.out.println(updateResponse.status());
    }

    // 删除文档
    @Test
    void testDeleteDocument() throws IOException {
        DeleteRequest deleteRequest = new DeleteRequest(ES_INDEX, "2");
        deleteRequest.timeout("1s");
        DeleteResponse deleteResponse = client.delete(deleteRequest, RequestOptions.DEFAULT);
        System.out.println(deleteResponse.status());
    }

    // 特殊的 真的项目一般都会批量插入数据
    @Test
    void testBulkRequest() throws IOException {
        BulkRequest bulkRequest = new BulkRequest();
        bulkRequest.timeout("10s");

        List<User> userList = new ArrayList<>();
        userList.add(new User("gblfy1", 2));
        userList.add(new User("gblfy2", 2));
        userList.add(new User("gblfy3", 2));
        userList.add(new User("gblfy4", 2));
        userList.add(new User("gblfy5", 2));

        //    批量处理请求
        for (int i = 0; i < userList.size(); i++) {
            // 飘零更新和批量删除，就在这里修改对应的请求就可以了
            bulkRequest.add(new IndexRequest("gblfy2")
                    .id("" + (i + 1))//id如果排序建议设置id，默认大数据量自动生成长ID
                    .source(JSON.toJSONString(userList.get(i)), XContentType.JSON));
        }
        BulkResponse bulkResponse = client.bulk(bulkRequest, RequestOptions.DEFAULT);
        // 是否失败
        System.out.println(bulkResponse.hasFailures());
    }

    // 查询
    // SearchRequest 搜索请求
    //SearchSourceBuilder条件构造
    //HighlightBuilder 构建高亮
    //TermQueryBuilder 精确查询
    //MatchAllQueryBuilder匹配所有
    //xxxQueryBuilder 刚才看到的命令！
    @Test
    void testSearch() throws IOException {
        SearchRequest searchRequest = new SearchRequest(ES_INDEX);
        //    构建搜索条件
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

        //    查询条件 我们可以使用
        // QueryBuilders.termQuery 精确匹配
        // QueryBuilders.matchAllQuery() 匹配所有
        // searchSourceBuilder.highlighter() 构建高亮
        TermQueryBuilder termQueryBuilder = QueryBuilders.termQuery("name", "zs");
        searchSourceBuilder.query(termQueryBuilder);
        //设置分页
        searchSourceBuilder.from();
        searchSourceBuilder.size();
        searchSourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));

        searchRequest.source(searchSourceBuilder);
        SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);

        System.out.println(JSON.toJSONString(searchResponse.getHits()));
        System.out.println("********************************");

        for (SearchHit documentFields : searchResponse.getHits().getHits()) {
            System.out.println(documentFields.getSourceAsMap());
        }
    }
}
