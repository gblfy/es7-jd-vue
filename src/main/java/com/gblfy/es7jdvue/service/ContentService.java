package com.gblfy.es7jdvue.service;

import com.alibaba.fastjson.JSON;
import com.gblfy.es7jdvue.consts.ESConst;
import com.gblfy.es7jdvue.pojo.Content;
import com.gblfy.es7jdvue.utils.HtmlParseUtil;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.core.TimeValue;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.Operator;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 搜索逻辑处理层
 *
 * @author gblfy
 * @date 2021-12-02
 */
@Service
public class ContentService {


    @Autowired
    private RestHighLevelClient restHighLevelClient;

    //1.解析数据放入es索引中
    public Boolean parseContent(String keyword) throws IOException {

        List<Content> contentList = new HtmlParseUtil().parseJD(keyword);
        // 把查询道德数据放入es
        BulkRequest bulkRequest = new BulkRequest();
        bulkRequest.timeout(ESConst.BULK_REQUEST_TIMEOUT);

        for (int i = 0; i < contentList.size(); i++) {
            bulkRequest.add(new IndexRequest(ESConst.JD_SEARCH_INDEX)
                    .source(JSON.toJSONString(contentList.get(i)), XContentType.JSON));
        }
        BulkResponse bulk = restHighLevelClient.bulk(bulkRequest, RequestOptions.DEFAULT);
        return !bulk.hasFailures();

    }

    //    2. 获取es中的数据，实现基本搜索功能
    public List<Map<String, Object>> searchPage(String keyword, int pageNo, int pageSize) throws IOException {
        if (pageNo <= 1) {
            pageNo = 1;
        }
        //    条件搜索
        SearchRequest searchRequest = new SearchRequest(ESConst.JD_SEARCH_INDEX);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

        //    分页
        searchSourceBuilder.from(pageNo);
        searchSourceBuilder.size(pageSize);

        //    精准匹配
        TermQueryBuilder termQueryBuilder = QueryBuilders.termQuery(ESConst.SEARCH_CONDITION_FIELD, keyword);
        searchSourceBuilder.query(termQueryBuilder);
        searchSourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));

        //    执行搜索
        searchRequest.source(searchSourceBuilder);
        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);

        //    解析结果
        ArrayList<Map<String, Object>> list = new ArrayList<>();
        for (SearchHit documentFields : searchResponse.getHits().getHits()) {
            list.add(documentFields.getSourceAsMap());
        }
        return list;
    }

    //    2. 获取es中的数据，实现基本搜索高亮功能
    public List<Map<String, Object>> searchPageHighlight(String keyword, int pageNo, int pageSize) throws IOException {
        if (pageNo <= 1) {
            pageNo = 1;
        }
        //    条件搜索
        SearchRequest searchRequest = new SearchRequest(ESConst.JD_SEARCH_INDEX);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

        //    分页
        searchSourceBuilder.from(pageNo);
        searchSourceBuilder.size(pageSize);

        //    精准匹配
        // TermQueryBuilder termQueryBuilder = QueryBuilders.termQuery(ESConst.SEARCH_CONDITION_FIELD, keyword);
        // searchSourceBuilder.query(termQueryBuilder);
        // searchSourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));

        //关键词分词
        MatchQueryBuilder matchQueryBuilder = QueryBuilders.matchQuery(ESConst.SEARCH_CONDITION_FIELD, keyword);
        searchSourceBuilder.query(matchQueryBuilder);
        searchSourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));

        //构建高亮
        HighlightBuilder highlightBuilder = new HighlightBuilder();
        highlightBuilder.field(ESConst.HIGHLIGHT_TITLE);
        highlightBuilder.field("desc");
        highlightBuilder.requireFieldMatch(true);//多个高亮 显示
        highlightBuilder.preTags(ESConst.HIGHLIGHT_PRE_TAGS);
        highlightBuilder.postTags(ESConst.HIGHLIGHT_POST_TAGS);
        searchSourceBuilder.highlighter(highlightBuilder);


        //    执行搜索
        searchRequest.source(searchSourceBuilder);
        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);

        //    解析结果
        ArrayList<Map<String, Object>> list = new ArrayList<>();
        for (SearchHit hit : searchResponse.getHits().getHits()) {
            //  解析高亮的字段，将原来的字段置换为我们高亮的字段即可！
            Map<String, HighlightField> highlightFields = hit.getHighlightFields();
            HighlightField title = highlightFields.get(ESConst.HIGHLIGHT_TITLE);
            HighlightField desc = highlightFields.get("desc");
            //    获取原来的结果
            Map<String, Object> sourceAsMap = hit.getSourceAsMap();
            if (title != null) {
                Text[] fragments = title.fragments();
                String newTitle = "";
                for (Text text : fragments) {
                    newTitle += text;
                }
                //高亮字段替换掉原来的内容即可
                sourceAsMap.put(ESConst.SEARCH_CONDITION_FIELD, newTitle);
            }
            if (desc != null) {
                Text[] fragments = desc.fragments();
                String newDesc = "";
                for (Text text : fragments) {
                    newDesc += text;
                }
                //高亮字段替换掉原来的内容即可
                sourceAsMap.put("desc", newDesc);
            }
            // 将结果放入list容器返回
            list.add(sourceAsMap);
        }
        return list;
    }

}
