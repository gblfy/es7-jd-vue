package com.gblfy.es7jdvue.consts;

/**
 * 搜索常量抽取
 *
 * @author gblfy
 * @date 2021-12-02
 */
public class ESConst {

    //拉取数据url前缀
    public static final String PULL_DATA_BASEURL = "https://search.xxx.com/Search?keyword=";
    //拉取商品数据标签
    public static final String PULL_GOOD_DATA_TAG ="J_goodsList";
    //商品数据标签中元素标签
    public static final String PULL_GOOD_DATA_CHILD_TAG ="li";


    //京东搜索数据索引
    public static final String JD_SEARCH_INDEX = "jd_goods";
    //高亮标题
    public static final String HIGHLIGHT_TITLE = "title";
    //高亮标签前缀
    public static final String HIGHLIGHT_PRE_TAGS = "<span style='color:red'>";
    //高亮标签后缀
    public static final String HIGHLIGHT_POST_TAGS = "</span>";
    //搜索挑条件字段
    public static final String SEARCH_CONDITION_FIELD = "title";
    public static final String BULK_REQUEST_TIMEOUT = "2m";

}
