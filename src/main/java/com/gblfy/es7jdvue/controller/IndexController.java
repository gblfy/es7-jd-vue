package com.gblfy.es7jdvue.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 主页面
 *
 * @author gblfy
 * @date 2021-12-02
 */
@Controller
public class IndexController {

    @GetMapping({"/index"})
    public String index() {
        return "index";
    }
}
