package com.gblfy.es7jdvue.utils;

import com.gblfy.es7jdvue.pojo.Content;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Component
public class HtmlParseUtil {

    public static void main(String[] args) throws IOException {
        //    获取请求 https://search.jd.com/Search?keyword=java
        String keyword = "java";
        new HtmlParseUtil().parseJD(keyword).forEach(System.out::println);
    }

    public List<Content> parseJD(String keyword) throws IOException {
        String baseUrl = "https://search.xxx.com/Search?keyword=";

        // 解析网页  在线预览中有处理
        Document document = Jsoup.parse(new URL(baseUrl + keyword), 30000);
        // 所有js中可以使用的方法，这里都能用
        Element element = document.getElementById("J_goodsList");
        //获取搜有的li元素
        Elements elements = element.getElementsByTag("li");

        ArrayList<Content> goodList = new ArrayList<>();
        // 获取元素中的内容，这里el 就是每一个li标签了
        for (Element el : elements) {
            //关于这种图片特别多的网站，所有的图片都是拉加载的
            String img = el.getElementsByTag("img").eq(0).attr("data-lazy-img");
            String price = el.getElementsByClass("p-price").eq(0).text();
            String title = el.getElementsByClass("p-name").eq(0).text();

            Content content = new Content();
            content.setTitle(title);
            content.setImg(img);
            content.setPrice(price);
            content.setDesc(title);
            goodList.add(content);
        }
        return goodList;
    }
}
