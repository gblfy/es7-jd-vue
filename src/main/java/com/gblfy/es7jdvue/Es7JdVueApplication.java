package com.gblfy.es7jdvue;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Es7JdVueApplication {

    public static void main(String[] args) {
        SpringApplication.run(Es7JdVueApplication.class, args);
    }

}
