package com.gblfy.es7jdvue.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Content {

    private String title;
    private String img;
    private String price;
    private  String desc;
}
